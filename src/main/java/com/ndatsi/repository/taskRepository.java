package com.ndatsi.repository;

import com.ndatsi.model.Priority;
import com.ndatsi.model.task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface taskRepository   extends JpaRepository<task, Long> {
    List<task> findByPriority(Priority priority);

}
