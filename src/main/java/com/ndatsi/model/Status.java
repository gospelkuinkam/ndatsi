package com.ndatsi.model;


public enum Status {
    IN_PROGRESS("en cours"),
    COMPLETED("terminée"),
    PENDING("en attente");

    private String label;

    Status(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}