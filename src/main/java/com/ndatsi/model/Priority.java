package com.ndatsi.model;

public enum Priority {
    LOW("Faible"),
    MEDIUM("Moyenne"),
    HIGH("Élevée");

    private String label;

    Priority(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}