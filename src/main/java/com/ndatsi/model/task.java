package com.ndatsi.model;
import java.time.LocalDate;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import com.ndatsi.model.Status;
import com.ndatsi.model.Priority;

import jakarta.persistence.*;

@Entity
@Table(name = "tasks")
public class task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String description;

    private String state;

    @Column(name = "creation_date")
    private LocalDate creationDate;

    @Column(name = "limit_date")
    private Date dueDate;
    @Enumerated(EnumType.STRING)
    private Priority priority;
    private String Comment;
    @Enumerated(EnumType.STRING)
    private Status status;


    public task() {
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public task(String title, String description, String state, LocalDate creationDate, Date dueDate, Priority priority, String comment, Status status) {
        this.title = title;
        this.description = description;
        this.state = state;
        this.creationDate = creationDate;
        this.dueDate = dueDate;
        this.priority = priority;
        Comment = comment;
        this.status = status;
    }

    //    @ManyToOne
//    private User assignedTo;
//
//    @OneToMany(mappedBy = "task", cascade = CascadeType.ALL)
//    private List<Comment> comments = new ArrayList<>();
}