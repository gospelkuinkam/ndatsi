package com.ndatsi.controller;
import com.ndatsi.model.Priority;
import com.ndatsi.model.task;
import com.ndatsi.repository.taskRepository;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/task")
public class taskController {
    private taskRepository taskRepository;
    public taskController(com.ndatsi.repository.taskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @GetMapping
    public List<task> getTask() {
        return taskRepository.findAll();
    }

    @PostMapping
    public ResponseEntity createTask(@RequestBody task task) throws URISyntaxException {
        task NewTask = taskRepository.save(task);
        return ResponseEntity.created(new URI("/task/" + NewTask.getId())).body("Enregister avec sucess");
    }

    @PutMapping("/{id}")
    public ResponseEntity updateTask(@PathVariable Long id, @RequestBody task task) {
        task currentTask = taskRepository.findById(id).orElseThrow(RuntimeException::new);
        currentTask.setDescription(task.getDescription());
        currentTask.setTitle(task.getTitle());
        currentTask.setState(task.getState());
        currentTask.setCreationDate(task.getCreationDate());
        currentTask = taskRepository.save(task);
        return ResponseEntity.ok("la tache à modifier avec sucess");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteTask(@PathVariable Long id) {
        taskRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{priority}")
    public List<task> getTasksByPriority(@PathVariable String priority) {
        Priority taskPriority = Priority.valueOf(priority.toUpperCase());
        return taskRepository.findByPriority(taskPriority);
    }

}



