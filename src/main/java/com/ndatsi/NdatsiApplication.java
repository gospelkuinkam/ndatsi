package com.ndatsi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NdatsiApplication {

    public static void main(String[] args) {
        SpringApplication.run(NdatsiApplication.class, args);
    }

}
